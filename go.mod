module gitlab.com/cratermoon/gonia

go 1.14

require (
	github.com/labstack/echo/v4 v4.2.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/cratermoon/trbsvc v1.0.0
)
