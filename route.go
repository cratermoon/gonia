package gonia

import (
	"github.com/labstack/echo/v4"
)

// Route describes an http route and handler with path and a handler function.
type Route struct {
	path        string
	name        string
	handlerFunc echo.HandlerFunc
}

// NewRoute creates a new route. The name will be set to the path value
func NewRoute(p string, h echo.HandlerFunc) Route {
	return NewRouteNamed(p, p, h)
}

// NewRouteNamed creates a new route with the given path, name, and handlerFunc.
func NewRouteNamed(p, n string, h echo.HandlerFunc) Route {
	return Route{
		path:        p,
		name:        n,
		handlerFunc: h,
	}
}
