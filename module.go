package gonia

// The Module interface defines the methods that all gonia modules must implement.
// Init is called on server startup, before handling connections
// Cleanup is called on server shutdown, before closing the server
type Module interface {
	Name() string
	Init(s *Server) error
	Cleanup(s *Server) error
}
