# Gonia

![gonia](gonia.png)

A helper library for building web servers with the LabStack Echo framework

## Description

Provides a way to build a server with additional services, called Modules, to provide functionality

Pass a value of a type that implements the `Module` interface  to `AddMod()` before calling `Start()` on the server to have gonia manage its lifecycle

## Getting Started

### Dependencies

* [labstack echo](https://echo.labstack.com/) v4
* go 1.14 or compatible

### Installing

* add gitlab.com/cratermoon/gonia to your go.mod

### Executing 

* Basic Usage

```
myModule := MyCoolMod()
s := gonia.New()
s.AddMod(myModule)

s.Start()

...

s.Shutdown()
```

## Structure

### Routes


### Modules

The Module interface has three methods

`Name() string`
`Init(s *Server) error`
`Cleanup(s *Server) error`

When the server's `Start()` method is called, it will call all the `Init` methods of configured modules, in no particular order. If any of the Init methods returns an error, it will be place on the channel provided to server `Start`.

The server uses the `Name()` method for logging and monitoring.

At server shutdown, each module will get a `Cleanup` call. TODO: context with timeout, so a hung module doesn't hang up the server)

### Middleware

The Middleware interface has one method, `Handler() func(next echo.HandlerFunc) echo.HandlerFunc`

## Help

`Start()` doesn't return. If you want to call Shutdown() you'll need a mechanism

```go
go func() {
    done := make(chan error)
    s.Start(done)
    ("Start: %s\n", <-done)
}
}()

// Wait for interrupt signal to gracefully shutdown the server with
// a timeout of 5 seconds.
quit := make(chan os.Signal)
// kill (no param) default send syscall.SIGTERM
// kill -2 is syscall.SIGINT
// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
<-quit
log.Println("Shutting down server...")

// The context is used to inform the server it has 5 seconds to finish
// the request it is currently handling
ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
defer cancel()

// if you don't have a Context, you can just call s.Shutdown()
if err := s.ShutdownCtx(ctx); err != nil {
	log.Fatal("Server forced to shutdown:", err)
}

log.Println("Server exiting")
```

## Authors

Contributors names and contact info

- Steven E. Newton

## Version History

* 0.0.1
    * Initial Release

## License

This project is licensed under the [GNU Affero](LICNESE) License - see the LICENSE file for details

## Acknowledgments

