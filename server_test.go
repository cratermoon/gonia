package gonia_test

import (
	"errors"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cratermoon/gonia"
)

func TestCreateServer(t *testing.T) {
	router := echo.New()
	testServer := gonia.NewWithRouter(t.Name(), router)
	l := testServer.Logger()
	assert.NotNilf(t, l, "Logger should not be nil")
	assert.Equal(t, router.Logger, l)
}

func TestStartServer(t *testing.T) {
	router := echo.New()
	testServer := gonia.NewWithRouter(t.Name(), router)
	done := make(chan error, 1)
	testServer.StartOn(":8080", done)
	assert.Nilf(t, <-done, "Server didn't exit properly")
	testServer.Shutdown()
}

type testModule struct {
	name         string
	initalized   int
	cleanedUp    int
	askedForName int
        mockErr  error
}

func (tm *testModule) Init(s *gonia.Server) error {
	tm.initalized++
	return tm.mockErr
}

func (tm *testModule) Cleanup(s *gonia.Server) error {
	tm.cleanedUp++
	return nil
}

func (tm *testModule) Name() string {
	tm.askedForName++
	return tm.name
}

func TestModInit(t *testing.T) {
	testMod := testModule{
		name: t.Name(),
	}
	testServer := gonia.New(t.Name())
	testServer.AddMod(&testMod)

	done := make(chan error, 1)

	assert.Equalf(t, 0, testMod.initalized, "Module Init should not have been called")
	testServer.StartOn(":8081", done)
	assert.Nilf(t, <-done, "Server didn't exit properly")
	assert.Equalf(t, 1, testMod.askedForName, "You never even called me by name")
	assert.Equalf(t, 1, testMod.initalized, "Module Init not called")
	assert.Equalf(t, 0, testMod.cleanedUp, "Module cleanup should not have happened")
	testServer.Shutdown()
	assert.Equalf(t, 1, testMod.cleanedUp, "Module cleanup should have happened")
}

func TestMultiMod(t *testing.T) {
	testServer := gonia.New(t.Name())
	testMod := testModule{
		name: t.Name(),
	}
	testServer.AddMod(&testMod)
	testMod2 := testModule{
		name: t.Name() + "2",
	}
	testMod2.mockErr = errors.New(testMod2.name)
	testServer.AddMod(&testMod2)

	done := make(chan error, 2)

	assert.Equalf(t, 0, testMod.initalized, "Module Init should not have been called")
	testServer.StartOn(":8082", done)
	for err := range done {
		if err != nil {
			assert.Equalf(t, testMod2.name, err.Error(), "Server didn't exit properly")
		}
	}
	assert.Equalf(t, 1, testMod.askedForName, "You never even called me by name")
	assert.Equalf(t, 1, testMod.initalized, "Module Init not called")
	assert.Equalf(t, 0, testMod.cleanedUp, "Module cleanup should not have happened")
	testServer.Shutdown()
	assert.Equalf(t, 1, testMod.cleanedUp, "Module cleanup should have happened")
}
func TestModShutdown(t *testing.T) {
	testMod := testModule{
		name: t.Name(),
	}
	testServer := gonia.New(t.Name())
	testServer.AddMod(&testMod)
	testServer.Shutdown()
	assert.Equalf(t, 0, testMod.askedForName, "That's my name, don't wear it out")
	assert.Equalf(t, 0, testMod.initalized, "Module Init should not have been called")
	assert.Equalf(t, 1, testMod.cleanedUp, "Module cleanup should have happened")
}
