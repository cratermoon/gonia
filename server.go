package gonia

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/labstack/echo/v4"
)

// Server is the top-level gonia object
type Server struct {
	id      string
	modules []Module
	router  *echo.Echo
	running bool
	mu      sync.RWMutex
}

// New creates a new server with the given id
func New(id string) *Server {
	return NewWithRouter(id, echo.New())
}

// NewWithRouter creates a new server given an existing router
func NewWithRouter(id string, router *echo.Echo) *Server {
	router.HideBanner = true
	s := Server{
		id:      id,
		modules: make([]Module, 0),
		router:  router,
	}

	return &s
}

// Logger returns the underlying echo.Logger
func (s *Server) Logger() echo.Logger {
	return s.router.Logger
}

// AddGET configures the given Route for the GET method
func (s *Server) AddGET(r Route) string {
	s.mu.Lock()
	defer s.mu.Unlock()

	route := s.router.GET(r.path, r.handlerFunc)
	route.Name = fmt.Sprintf("%s.get-%s", s.id, r.name)

	return route.Name
}

// AddPOST configures the given Route for the POST method
func (s *Server) AddPOST(r Route) string {
	s.mu.Lock()
	defer s.mu.Unlock()

	route := s.router.POST(r.path, r.handlerFunc)
	route.Name = fmt.Sprintf("%s.get-%s", s.id, r.name)

	return route.Name
}

// AddMod installs a Module on the list of modules to be initialized and cleaned up buy the server. This must be called before Start()
func (s *Server) AddMod(m Module) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.modules = append(s.modules, m)
}

// UseMiddleware will install the Middleware for all paths
func (s *Server) UseMiddleware(m Middleware) {
	s.router.Use(m.Handler())
}

// Start begins serving on port 8080
// The server will call the Init method on any installed Modules before serving
// The done channel will have the error if startup fails.
func (s *Server) Start(done chan error) {
	s.StartOn(":8080", done)
}

// StartOn begins serving on the given port.
// The server will call the Init method on any installed Modules before serving
// The done channel will have the error if startup fails.
func (s *Server) StartOn(port string, initError chan error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	for _, m := range s.modules {
		s.router.Logger.Debugf("initializing module %s", m.Name())

		err := m.Init(s)
		if err != nil {
			s.router.Logger.Warnf("initialization of module %s returned error %s", m.Name(), err)
			initError <- err
		}
	}

	go func() {
		s.router.Logger.Infof("starting the %s server on port %s", s.id, port)

		if err := s.router.Start(port); err != nil && err != http.ErrServerClosed {
			s.router.Logger.Fatalf("error %s shutting down the server", err)
		}
	}()
	initError <- nil
	close(initError)
	s.running = true
}

// Shutdown is a convenience method that calls shuts down the server with a nil Context
func (s *Server) Shutdown() {
	s.ShutdownWithContext(nil)
}

// Shutdown stops the server. Before stopping, the server will call the Cleanup method on any installed Modules
// if parent is nil the function will use context.Background()
func (s *Server) ShutdownWithContext(parent context.Context) {
	s.mu.Lock()
	defer s.mu.Unlock()

	for _, m := range s.modules {
		err := m.Cleanup(s)
		if err != nil {
			s.router.Logger.Warnf("Shutdown of module %s returned error %s", m.Name(), err)
		}
	}

	if parent == nil {
		parent = context.Background()
	}
	ctx, cancel := context.WithTimeout(parent, 10*time.Second)
	defer cancel()

	if err := s.router.Shutdown(ctx); err != nil {
		s.router.Logger.Fatal(err)
	}
	s.running = false
	s.router.Logger.Debugf("gonia server %s shutdown complete", s.id)
}
