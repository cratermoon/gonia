package trace

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/cratermoon/trbsvc/pkg/trb"
)

var rb trb.RingTrace

type TraceMiddleware struct {
	rb *trb.RingTrace
}

func New() TraceMiddleware {
	return TraceMiddleware{
		rb: trb.NewRingTrace(),
	}
}

// Trace is the echo middleware handler func e.Use(Trace)
func trace(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		rb.Append(c.Path())
		return next(c)
	}
}

func (tm TraceMiddleware) Handler(next echo.HandlerFunc) func(next echo.HandlerFunc) echo.HandlerFunc {
	return trace
}
