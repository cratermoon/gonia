package gonia

import (
	"github.com/labstack/echo/v4"
)

// Middleware defines a method for the server to find the handler function
type Middleware interface {
	Handler() func(next echo.HandlerFunc) echo.HandlerFunc
}
